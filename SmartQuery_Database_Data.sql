-- MySQL dump 10.13  Distrib 5.1.47, for Win32 (ia32)
--
-- Host: localhost    Database: smartquery
-- ------------------------------------------------------
-- Server version	5.1.47-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

drop database if exists smartquery;
create database smartquery;
use smartquery;

--
-- Table structure for table `querymaster`
--

DROP TABLE IF EXISTS `querymaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `querymaster` (
  `Query_ID` int(11) NOT NULL AUTO_INCREMENT,
  `User_ID` int(11) DEFAULT NULL,
  `Query` varchar(1000) DEFAULT NULL,
  `Create_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Query_ID`),
  KEY `fk_QueryMaster_UserMaster` (`User_ID`),
  CONSTRAINT `fk_QueryMaster_UserMaster` FOREIGN KEY (`User_ID`) REFERENCES `usermaster` (`User_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `querymaster`
--

LOCK TABLES `querymaster` WRITE;
/*!40000 ALTER TABLE `querymaster` DISABLE KEYS */;
INSERT INTO `querymaster` VALUES (2,1,'select * from usermaster','2012-05-16 10:05:09'),(3,1,'select * from usermaster','2012-05-16 10:05:17'),(4,1,'select * from usermaster','2012-05-16 10:10:21');
/*!40000 ALTER TABLE `querymaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usermaster`
--

DROP TABLE IF EXISTS `usermaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usermaster` (
  `User_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(45) DEFAULT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `User_Type` varchar(20) DEFAULT NULL,
  `User_Status` varchar(20) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Contact_Number` varchar(20) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`User_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usermaster`
--

LOCK TABLES `usermaster` WRITE;
/*!40000 ALTER TABLE `usermaster` DISABLE KEYS */;
INSERT INTO `usermaster` VALUES (1,'admin','admin','Administrator','Active','Rohan','01724565654','admin@gmail.com'),(2,'employee','employee','Employee','Active','Aakash','98888989898','employee@gmail.com'),(3,'User','user','Employee','Active','Rajpreet','9899988888','user@gmail.com'),(4,'Ajay','ajay','Employee','Active','Ajay Kumar','9856742135','Employee@gmail.com');
/*!40000 ALTER TABLE `usermaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userprivilegemaster`
--

DROP TABLE IF EXISTS `userprivilegemaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userprivilegemaster` (
  `User_ID` int(11) NOT NULL,
  `Can_Retrieve` bit(1) DEFAULT NULL,
  `Can_Insert` bit(1) DEFAULT NULL,
  `Can_Update` bit(1) DEFAULT NULL,
  `Can_Delete` bit(1) DEFAULT NULL,
  `Can_Drop` bit(1) DEFAULT NULL,
  `Can_Create` bit(1) DEFAULT NULL,
  `Can_Alter` bit(1) DEFAULT NULL,
  PRIMARY KEY (`User_ID`),
  KEY `fk_UserPriviledgeMaster_UserMaster` (`User_ID`),
  CONSTRAINT `fk_UserPriviledgeMaster_UserMaster` FOREIGN KEY (`User_ID`) REFERENCES `usermaster` (`User_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userprivilegemaster`
--

LOCK TABLES `userprivilegemaster` WRITE;
/*!40000 ALTER TABLE `userprivilegemaster` DISABLE KEYS */;
INSERT INTO `userprivilegemaster` VALUES (1,'\0','','','','','','\0'),(2,'','','','','','','\0'),(3,'','','','','','','\0');
/*!40000 ALTER TABLE `userprivilegemaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userqueryactivity`
--

DROP TABLE IF EXISTS `userqueryactivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userqueryactivity` (
  `Query_Activity_ID` int(11) NOT NULL AUTO_INCREMENT,
  `User_ID` int(11) DEFAULT NULL,
  `Query` varchar(1000) DEFAULT NULL,
  `Query_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Query_Activity_ID`),
  KEY `fk_UserQueryActivity_UserMaster` (`User_ID`),
  CONSTRAINT `fk_UserQueryActivity_UserMaster` FOREIGN KEY (`User_ID`) REFERENCES `usermaster` (`User_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userqueryactivity`
--

LOCK TABLES `userqueryactivity` WRITE;
/*!40000 ALTER TABLE `userqueryactivity` DISABLE KEYS */;
INSERT INTO `userqueryactivity` VALUES (12,1,'select * from usermaster;','2012-05-16 10:04:10'),(13,1,'select * from usermaster;','2012-05-16 10:04:49'),(14,1,'select * from usermaster','2012-05-16 10:05:05'),(15,1,'select * from usermaster','2012-05-16 10:05:08'),(16,1,'select * from usermaster','2012-05-16 10:05:16'),(17,1,'select * from usermaster','2012-05-16 10:05:17'),(18,1,'select * from usermaster','2012-05-16 10:07:55'),(19,1,'select * from usermaster','2012-05-16 10:09:38'),(20,1,'select * from usermaster','2012-05-18 12:48:30'),(21,1,'select * from usermaster','2012-05-18 12:48:46');
/*!40000 ALTER TABLE `userqueryactivity` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-05-27 11:32:13
