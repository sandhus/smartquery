SmartQuery
==========

A tool to manage all mysql databases from single place.

Following folders are present :
1.	code  :  It contains the source code of project



Following documents are present :
1.	SmartQuery_DM.pdf  : It contains the datamodel of the project in pdf format.

2.	SmartQuery_DM.png : It contains the datamodel of the project in png format.

3.	SmartQuery_SRS.pdf : It contains the information regarding project.

4.	SmartQuery_DFD : It contains the information regarding Data Flow Diagram of project.

5.	SmartQuery_Database_Data.sql : It contains the database of the project.

6.	GiveAuthenticationToRemoteUsersInMySQL.doc  : Steps to grant access to  client application to the mysql server on server machine. (This step is optional)


Steps to run the project : 
1.	Install all the required softwares which were used during training.
2.	Create the database by executing the script file. Following command is to be used in the command prompt after taking the control to the Database folder.

mysql -u  root < SmartQuery_Database_Data.sql

3.	Similarly, Create the database having image data  using following command : 
		mysql -u  root < imagestore.sql

4.	Now open and run the project using NetBeans.

5.	Use following username and password :

username : admin
password : admin

username : employee
password: employee

¬¬¬¬¬¬

6.	After Logging in click on the ‘DBrowse’  menuitem in the ‘File’ menu.


7.	Give 'localhost' as host server.’3306’ as 'Port Number' and ‘root’ as 'username' and give password as applicable. Click on Connect. Now execute queries and see details of databases and data in tables by just clicking on the table names.


