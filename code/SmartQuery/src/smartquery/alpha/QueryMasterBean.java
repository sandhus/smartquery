/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package smartquery.alpha;

/**
 *
 * @author admin
 */
public class QueryMasterBean {
    private int userId;
    private int queryId;
    private String createDate;
    private String query;

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    

}
