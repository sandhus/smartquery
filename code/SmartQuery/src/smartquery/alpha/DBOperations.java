/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smartquery.alpha;


import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.ImageIcon;

/**
 *This class contains the methods that are being used by user to perform various operations
 */
public class DBOperations {
     Connection dbrowseConnection;
    //----------------------------------------------------------------------
    //                               Login Related
    //----------------------------------------------------------------------
   /**
     * This method authenticates the user while logging in and will return the reference of type UsermasterBean
     * @param userName
     * @param password
     * @return UsermasterBean
     */
     public UsermasterBean authenticateUser(String userName, String password) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        UsermasterBean objBean = null;
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from usermaster where Username = ?");
            pstmt.setString(1, userName);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                if (rs.getString("password").equals(password)) {
                    objBean = new UsermasterBean();
                    objBean.setUserId(rs.getInt("User_ID"));
                    objBean.setUsername(rs.getString("Username"));
                    objBean.setPassword(rs.getString("Password"));
                    objBean.setUserType(rs.getString("User_Type"));
                    objBean.setUserStatus(rs.getString("User_Status"));
                    objBean.setName(rs.getString("Name"));
                    objBean.setContactNumber(rs.getString("Contact_Number"));
                    objBean.setEmail(rs.getString("Email"));
                }
            }
        } catch (Exception e) {
            System.out.println("authenticateUser(String userName, String password) : of DBoperations" + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("authenticateUser(String userName, String password) : of DBoperations" + e);
            }
        }
        return objBean;
    }
/**
     * This method gives the user detail according to username and returns the reference of type UsermasterBean 
     * @param userName
     * @return UsermasterBean
     */
    public UsermasterBean getUserDetailByUsername(String userName) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        UsermasterBean objBean = null;
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from usermaster where Username = ?");
            pstmt.setString(1, userName);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                objBean = new UsermasterBean();
                objBean.setUserId(rs.getInt("User_ID"));
                objBean.setUsername(rs.getString("Username"));
                objBean.setPassword(rs.getString("Password"));
                objBean.setUserType(rs.getString("User_Type"));
                objBean.setUserStatus(rs.getString("User_Status"));
                objBean.setEmail(rs.getString("Email"));
                objBean.setName(rs.getString("Name"));
                objBean.setContactNumber(rs.getString("Contact_Number"));
            }
        } catch (Exception e) {
            System.out.println("getUserDetailByUsername(String userName)  of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getUserDetailByUsername(String userName) of DBoperations : " + e);
            }
        }
        return objBean;
    }

    //-------------------------------------------------------------------------
    //                                 UserAccountDetail Related
    //-------------------------------------------------------------------------
/**
     * This method gives the UserNameList and returns the reference of type ArrayList 
     * @return ArrayList
     */
    public ArrayList getAllUserNameList() {
        Connection conn = null;
        ArrayList alstUser = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select Username from usermaster");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                alstUser.add(rs.getString("Username"));
            }
        } catch (Exception e) {
            System.out.println("getAllUserList() of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllUserList() of DBoperations : " + e);
            }
        }
        return alstUser;
    }
/**
     * This method gives the detail of all users and returns the reference of type ArrayList
     * @return ArrayList
     */
    public ArrayList getAllUserDetailList() {
        Connection conn = null;
        ArrayList alstUser = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select um.User_ID,Username,Password,User_Type,User_Status,Name ,Email,Contact_Number from usermaster um,userprivilegemaster upm where upm.User_ID=um.User_ID");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                {
                    UsermasterBean objBean = new UsermasterBean();
                    objBean.setUserId(rs.getInt("um.User_ID"));
                    objBean.setUsername(rs.getString("Username"));
                    objBean.setPassword(rs.getString("Password"));
                    objBean.setUserType(rs.getString("User_Type"));
                    objBean.setUserStatus(rs.getString("User_Status"));
                    objBean.setEmail(rs.getString("Email"));
                    objBean.setName(rs.getString("Name"));
                    objBean.setContactNumber(rs.getString("Contact_Number"));
                    alstUser.add(objBean);
                }
            }
        } catch (Exception e) {
            System.out.println("getAllUserDetailList() of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllUserDetailList() of DBoperations : " + e);
            }
        }
        return alstUser;
    }
/**
     * This method gives the detail of user account according to user id and returns the reference of type UsermasterBean
     * @param userId
     * @return UsermasterBean
     */
    public UsermasterBean getUserAccountDetailByUserId(int userId) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        UsermasterBean objBean = new UsermasterBean();
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select User_ID,Username,Password,User_Type,User_Status,Name, Email,Contact_Number from usermaster where User_Id = ?");
            pstmt.setInt(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                {

                    objBean.setUserId(rs.getInt("User_ID"));
                    objBean.setUsername(rs.getString("Username"));
                    objBean.setPassword(rs.getString("Password"));
                    objBean.setUserType(rs.getString("User_Type"));
                    objBean.setUserStatus(rs.getString("User_Status"));
                    objBean.setEmail(rs.getString("Email"));
                    objBean.setName(rs.getString("Name"));
                    objBean.setContactNumber(rs.getString("Contact_Number"));
                }
            }
        } catch (Exception e) {
            System.out.println("getUserDetailByUserId(int userId) of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getUserDetailByUserId(int userId) of DBoperations : " + e);
            }
        }
        return objBean;
    }
/**
     * This method gives the maximum UserId from the table and returns the refence of type integer
     * @return integer
     */
    public int getMaxUserId() {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int maxUserID = 0;
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select max(User_ID) from usermaster");
            rs = pstmt.executeQuery();

            if (rs.next()) {
                maxUserID = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println("getMaxUserId() of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getMaxUserId() of DBoperations : " + e);
            }
        }
        return maxUserID;
    }
/**
     * This method adds the user account detail and returns the reference of type String 
     * @param objBean
     * @return String
     */
    public String addUserAccountDetail(UsermasterBean objBean) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String result = "failed";
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from usermaster where Username = ?");
            pstmt.setString(1, objBean.getUsername());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                result = "exists";
            } else {

                pstmt = conn.prepareStatement("insert into usermaster ( User_ID ,Username ,Password,User_Type, User_Status ,Name , Email , Contact_Number) values(?,?,?,?,?,?,?,?) ");
                pstmt.setInt(1, objBean.getUserId());
                pstmt.setString(2, objBean.getUsername());
                pstmt.setString(3, objBean.getPassword());
                pstmt.setString(4, objBean.getUserType());
                pstmt.setString(5, objBean.getUserStatus());
                pstmt.setString(6, objBean.getName());
                pstmt.setString(7, objBean.getEmail());
                pstmt.setString(8, objBean.getContactNumber());
                
                int i = pstmt.executeUpdate();
                if (i > 0) {
                    result = "added";
                    pstmt = conn.prepareStatement("insert into userprivilegemaster (User_ID) values(?)");
                    pstmt.setInt(1, objBean.getUserId());
                    i=pstmt.executeUpdate();
                }
            }
        } catch (Exception e) {
            System.out.println("addUserAccountDetail(UsermasterBean objBean) of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("addUserAccountDetail(UsermasterBean objBean) of DBoperations : " + e);
            }
        }
        return result;
    }
/**
     * This method updates the user account detail and returns the reference of type String 
     * @param objBean
     * @return String
     */
    public String updateUserAccountDetail(UsermasterBean objBean) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String result = "failed";
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from usermaster where Username = ? and User_ID !=?");
            pstmt.setString(1, objBean.getUsername());
            pstmt.setInt(2, objBean.getUserId());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                result = "exists";
            } else {
                pstmt = conn.prepareStatement("update usermaster set Username = ?,Password=?,User_Type =?, User_Status = ? ,Name = ?, Email = ?, Contact_Number = ? where User_ID=?");
                pstmt.setString(1, objBean.getUsername());
                pstmt.setString(2, objBean.getPassword());
                pstmt.setString(3, objBean.getUserType());
                pstmt.setString(4, objBean.getUserStatus());
                pstmt.setString(5, objBean.getName());
                pstmt.setString(6, objBean.getEmail());
                pstmt.setString(7, objBean.getContactNumber());
                pstmt.setInt(8, objBean.getUserId());
                System.out.println(pstmt.toString());
                int i = pstmt.executeUpdate();
                if (i > 0) {
                    result = "updated";
                }
            }
        } catch (Exception e) {
            System.out.println("updateUserAccountDetail(UsermasterBean objBean) of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("updateUserAccountDetail(UsermasterBean objBean) of DBoperations : " + e);
            }
        }
        return result;
    }
/**
     * This method gives All Users Privilege Detail List and returns the reference of type ArrayList  
     * @return ArrayList
     */
     public ArrayList getAllUserPrivilegeDetailList() {
        Connection conn = null;
        ArrayList alstUser = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select um.User_ID,Can_Retrieve,Can_Insert,Can_Update,Can_Delete,Can_Drop,Can_Create,Can_Alter from usermaster um,userprivilegemaster upm where upm.User_ID=um.User_ID");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                {
                    UserPriviledgeMasterBean objBean = new UserPriviledgeMasterBean();
                    objBean.setUserId(rs.getInt("User_ID"));
                    objBean.setCanRetrieve(rs.getBoolean("Can_Retrieve"));
                    objBean.setCanInsert(rs.getBoolean("Can_Insert"));
                    objBean.setCanUpdate(rs.getBoolean("Can_Update"));
                    objBean.setCanDrop(rs.getBoolean("Can_Drop"));
                    objBean.setCanDelete(rs.getBoolean("Can_Delete"));
                    objBean.setCanCreate(rs.getBoolean("Can_Create"));
                    objBean.setCanAlter(rs.getBoolean("Can_Alter"));
                    alstUser.add(objBean);
                }
            }
        } catch (Exception e) {
            System.out.println("getAllUserPriviledgeDetailList() of DBoperations : " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllUserPriviledgeDetailList() of DBoperations : " + e);
            }
        }
        return alstUser;
    }
/**
      * This method updates the User PriviledgeDetail and returns the reference of type String 
      * @param objBean
      * @return String
      */
    
    public String updateUserPriviledgeDetail(UserPriviledgeMasterBean objBean) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        String result = "failed";
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("update userprivilegemaster set Can_Retrieve =? ,Can_Insert = ?,Can_Update = ?,Can_Delete = ?,Can_Drop = ?,Can_Create = ?,Can_Alter= ? where User_ID =?");
            pstmt.setBoolean(1, objBean.isCanRetrieve());
            pstmt.setBoolean(2, objBean.isCanInsert());
            pstmt.setBoolean(3, objBean.isCanUpdate());
            pstmt.setBoolean(4, objBean.isCanDelete());
            pstmt.setBoolean(5, objBean.isCanDrop());
            pstmt.setBoolean(6, objBean.isCanCreate());
            pstmt.setBoolean(7, objBean.isCanAlter());
            pstmt.setInt(8, objBean.getUserId());
            System.out.println(pstmt.toString());
            int i = pstmt.executeUpdate();
            if (i > 0) {
                result = "updated";
            }
        } catch (Exception e) {
            System.out.println("updateUserPriviledgeDetail(UserPriviledgeMasterBean objBean) of DBoperations : " + e);
        } finally {
            try {
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("updateUserPriviledgeDetail(UserPriviledgeMasterBean objBean) of DBoperations : " + e);
            }
        }
        return result;
    }

    //---------------------------------------------------------------------------------------
    //          Database Browser Related DBrowse.java
    //-----------------------------------------------------------------------------------------------
    /**
     * This method shows all the databases and returns the reference of type ArrayList
     * @return ArrayList
     */
    public ArrayList getAllDatabases() {

        Connection conn = null;
        ArrayList allDatabases = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("show databases");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                allDatabases.add(rs.getString(1));
            }

        } catch (Exception e) {
            System.out.println("getDatabases() of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getDatabases() of DBOperations: " + e);
            }
        }
        return allDatabases;
    }
/**
     * This method gives the User PriviledgeDetail according to User_Id and returns the reference of type UserPriviledgeMasterBean
     * @param userId
     * @return UserPriviledgeMasterBean
     */
    public UserPriviledgeMasterBean getUserPriviledgeDetails(int userId) {

        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        UserPriviledgeMasterBean objBean = null;
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from userprivilegemaster where user_id=?");
            pstmt.setInt(1, userId);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                objBean = new UserPriviledgeMasterBean();
                objBean.setCanRetrieve(rs.getBoolean("can_retrieve"));
                objBean.setCanInsert(rs.getBoolean("can_insert"));
                objBean.setCanUpdate(rs.getBoolean("can_update"));
                objBean.setCanDelete(rs.getBoolean("can_delete"));
                objBean.setCanDrop(rs.getBoolean("can_drop"));
                objBean.setCanCreate(rs.getBoolean("can_create"));
                objBean.setCanAlter(rs.getBoolean("can_alter"));

            }

        } catch (Exception e) {
            System.out.println("getUserPriviledgeDetails(int userId) of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getUserPriviledgeDetails(int userId) of DBOperations: " + e);
            }
        }
        return objBean;
    }

    /**
     * This method is used to insert the Execute Queries Record ,it returns nothing
     * @param userId
     * @param query 
     */
    
    public void insertExecutedQueryRecord(int userId, String query) {

        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("insert into userqueryactivity (User_ID, Query, Query_Date) values(?,?,?)");
            pstmt.setInt(1, userId);
            pstmt.setString(2, query);
            pstmt.setString(3, getCurrentDateTime());
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println("insertExecutedQueryRecord(int userId,String query) of DBOperations: " + e);
        } finally {
            try {
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("insertExecutedQueryRecord(int userId,String query) of DBOperations: " + e);
            }
        }
    }
/**
     * This method is used to insert the Saved Queries Record ,it returns nothing
     * @param userId
     * @param query 
     */
    
    public void insertSavedQueryRecord(int userId, String query) {

        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("insert into querymaster (User_ID, Query, create_Date) values(?,?,?)");
            pstmt.setInt(1, userId);
            pstmt.setString(2, query);
            pstmt.setString(3, getCurrentDateTime());
            pstmt.executeUpdate();

        } catch (Exception e) {
            System.out.println("insertSavedQueryRecord(int userId,String query) of DBOperations: " + e);
        } finally {
            try {
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("insertSavedQueryRecord(int userId,String query) of DBOperations: " + e);
            }
        }
    }

    //---------------------------------------------------------------------------------------
    //          Methods and Connections Being used to manage user activity in Database Browser
    //-----------------------------------------------------------------------------------------------
    /**
     * This method creates the connection for DBrowse and returns nothing
     */
    
    public String  makeConnectionForDBrowse(ConnectToServerBean objBean) {
        String result="s";
        try {
            
            Class.forName("com.mysql.jdbc.Driver");
         //   dbrowseConnection = DBConnection.getConnection();
             dbrowseConnection = DriverManager.getConnection("jdbc:mysql://"+objBean.getHostServer()+":"+objBean.getPort()+"/", objBean.getUserName(), objBean.getPassword());

        } catch (Exception e) {
            result=e.getMessage();
            System.out.println("Exception in makeConnectionForDBrowse() of DBConnection : " + e);
        }
        return result;
    }
 /**
     * This method creates the connection for DBrowse and returns nothing,it has String type arguments
     * @param currentDatabase 
     */
    public void makeConnectionForDBrowse(String currentDatabase) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            dbrowseConnection = DriverManager.getConnection("jdbc:mysql:///" + currentDatabase, "root", "");

        } catch (Exception e) {
            System.out.println("Exception in makeConnectionForDBrowse() of DBConnection : " + e);
        }
    }
/**
     * This method changes the database, it has no return type
     * @param db 
     */
    public void changeDatabase(String db) {
        Statement stmt = null;
        try {
            stmt = dbrowseConnection.createStatement();
            System.out.println("Database changed" + db);
            int d = stmt.executeUpdate("use " + db);

            if (d > 0) {
                System.out.println("Database changed");
            }
        } catch (Exception exp) {

            System.out.println("changeDatabase() of DBOperations: " + exp);
        } finally {
            try {
                //stmt.close();
            } catch (Exception e) {
                System.out.println("changeDatabase() of DBOperations: " + e);
            }
        }
    }
/**
     * this method shows all the tables of the selected database, it returns the reference of type ArrayList
     * @return ArrayList
     */
    public ArrayList getTableNames() {
        ArrayList alstTableNames = new ArrayList();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = dbrowseConnection.createStatement();
            rs = stmt.executeQuery("show tables");
            while (rs.next()) {
                alstTableNames.add(rs.getString(1));
            }
        } catch (Exception exp) {
            System.out.println("getTableNames()  of DBOperations: " + exp);
        } finally {
            try {
                stmt.close();
            } catch (Exception e) {
                System.out.println("getTableNames()  of DBOperations: " + e);
            }
        }
        return alstTableNames;
    }
/**
     * this method updates the query and returns the reference of type String
     * @param query
     * @return 
     */
    public String updateQuery(String query) {
        Statement stmt = null;
        String result = "failed";
        try {

            stmt = dbrowseConnection.createStatement();
            int rows = stmt.executeUpdate(query);
            if (rows >= 0) {
                result = "updated";
            }
        } catch (Exception exp) {
            result = exp.toString();
        }
        return result;
    }
/**
     * This method executes the query with the image,it returns the reference of type ArrayList
     * @param query
     * @return ArrayList
     */
    public ArrayList executeQueryWithImage(String query) {

        ArrayList alst = new ArrayList();
        ArrayList alstColumnNames = new ArrayList();
        ArrayList alstAllRow = new ArrayList();
        Statement stmt = null;
        ResultSet rs = null;
        try {

            stmt = dbrowseConnection.createStatement();
            rs = stmt.executeQuery(query);

            ResultSetMetaData rsmd1 = rs.getMetaData();
            int columnCount = rsmd1.getColumnCount();

            System.out.println("Count::" + columnCount);

            //Getting Column Names
            for (int a = 1; a <= columnCount; a++) {
                alstColumnNames.add(rsmd1.getColumnName(a));
            }

            //Getting Data
            while (rs.next()) {
                ArrayList alstRow = new ArrayList();
                for (int i = 1; i <= columnCount; i++) {
                    String columnType = rsmd1.getColumnTypeName(i);
                    System.out.println("ColumnType ::"+columnType);
                    //to add Image Data
                    if (columnType.contains("blob") || columnType.contains("BLOB")) {

                        String len = rs.getString(i);
                        try {
                            InputStream is = rs.getBinaryStream(i);
                            if (len != null && len.length() > 0) {
                                byte readImage[] = new byte[len.length()];
                                while (is.read(readImage) != -1) {
                                }
                                ImageIcon imgIcon = new ImageIcon(readImage);
                                alstRow.add(imgIcon);
                            } else {
                                alstRow.add(null);
                            }
                            is.close();
                        } catch (Exception e) {
                            System.out.println("1executeQueryWithImage(String query) of DBOperations : " + e);
                        }

                    }
                    else {
                        alstRow.add(rs.getString(i));
                    }
                }
                alstAllRow.add(alstRow);
            }

        } catch (Exception e) {
            //al = new ArrayList();
            //al.add(e.toString());
            System.out.println("2executeQueryWithImage(String query) of DBOperations : " + e);
        } finally {
            try {

                rs.close();
                stmt.close();

            } catch (Exception e) {
                System.out.println("3executeQueryWithImage(String query) of DBOperations : " + e);
            }
        }
        alst.add(alstColumnNames);
        alst.add(alstAllRow);
        return alst;
    }
/**
     * This method is used to update the image , it returns String
     * @param tableName
     * @param selectedColumn
     * @param selectedRow
     * @param is
     * @param currentQuery
     * @param currentDatabase
     * @return String
     */
    public String updateImage(String tableName, int selectedColumn, int selectedRow, InputStream is, String currentQuery, String currentDatabase) {


        try {
            System.out.println("1" + currentDatabase);
            DatabaseMetaData dbmd = dbrowseConnection.getMetaData();
            System.out.println("2" + currentQuery);
            ResultSet rs1 = dbmd.getPrimaryKeys(null, currentDatabase, tableName);

            System.out.println("rs11111111=="+rs1);

            System.out.println("3" + tableName);
            String primaryKeyName = "";

            if (rs1.next()) {

                primaryKeyName = rs1.getString(4);
            }
            System.out.println("4" + primaryKeyName);

            Statement stmt1 = dbrowseConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            ResultSet rs11 = stmt1.executeQuery(currentQuery);
            ResultSetMetaData rsmd1 = rs11.getMetaData();
            String columnName = rsmd1.getColumnName(selectedColumn + 1);
            rs11.absolute(selectedRow + 1);
            String primaryValue = rs11.getString(primaryKeyName);

            System.out.println("5" + primaryValue);

            PreparedStatement pstmt11 = dbrowseConnection.prepareStatement("update " + tableName + " set " + columnName + " = ? where " + primaryKeyName + "= ?");
            System.out.println("PStmt" + pstmt11.toString());
            pstmt11.setBlob(1, is);
            pstmt11.setString(2, primaryValue);

            System.out.println("PStmt " + pstmt11.toString());
            pstmt11.executeUpdate();

        } catch (Exception e) {
            System.out.println("updateImage(String tableName, int selectedColumn, int selectedRow, InputStream is, String currentQuery, String currentDatabase) " + e);
            return "failed";
        }
        return "success";
    }


    //---------------------------------------------------------------------------------------
    //          User Executed Query Related ExecutedQuery.java
    //-----------------------------------------------------------------------------------------------
    /**
     * This method shows the List of all the executed queries,it returns the reference of type ArrayList
     * @return ArrayList
     */
    
    public ArrayList getAllExecutedQueryList() {

        Connection conn = null;
        ArrayList alstAllExecutedQueries = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from userqueryactivity");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                UserQueryActivityBean objExecutedQueryBean = new UserQueryActivityBean();

                objExecutedQueryBean.setQueryId(rs.getString("Query_Activity_Id"));
                objExecutedQueryBean.setUserId(rs.getString("User_Id"));
                objExecutedQueryBean.setQuery(rs.getString("Query"));
                objExecutedQueryBean.setQueryDate(rs.getString("Query_Date"));

                alstAllExecutedQueries.add(objExecutedQueryBean);
            }

        } catch (Exception e) {
            System.out.println("getAllExecutedQueryList()  of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllExecutedQueryList()  of DBOperations: " + e);
            }
        }
        return alstAllExecutedQueries;
    }
 /**
     * This method shows the List of all the executed queries,it returns the reference of type ArrayList
     * @param User_Id
     * @return 
     */
    public ArrayList getAllExecutedQueryList(int User_Id) {

        Connection conn = null;
        ArrayList alstAllExecutedQueries = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from userqueryactivity where User_Id=?");
            pstmt.setInt(1, User_Id);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                UserQueryActivityBean objExecutedQueryBean = new UserQueryActivityBean();

                objExecutedQueryBean.setQueryId(rs.getString("Query_Activity_Id"));
                objExecutedQueryBean.setUserId(rs.getString("User_Id"));
                objExecutedQueryBean.setQuery(rs.getString("Query"));
                objExecutedQueryBean.setQueryDate(rs.getString("Query_Date"));

                alstAllExecutedQueries.add(objExecutedQueryBean);
            }

        } catch (Exception e) {
            System.out.println("getAllExecutedQueryList(int User_Id)  of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllExecutedQueryList(int User_Id)  of DBOperations: " + e);
            }
        }
        return alstAllExecutedQueries;
    }
    
    /**
     * This method shows the List of all the executed queries by name,it returns the reference of type ArrayList
     * @param username
     * @return ArrayList
     */
    public ArrayList getAllExecutedQueryListByUsername(String username) {

        Connection conn = null;
        ArrayList alstAllExecutedQueries = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select Query_Activity_Id,uqa.User_Id ,Query,Query_Date from userqueryactivity uqa,usermaster um where um.User_Id=uqa.User_Id and um.Username=?");
            pstmt.setString(1, username);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                UserQueryActivityBean objExecutedQueryBean = new UserQueryActivityBean();

                objExecutedQueryBean.setQueryId(rs.getString("Query_Activity_Id"));
                objExecutedQueryBean.setUserId(rs.getString("User_Id"));
                objExecutedQueryBean.setQuery(rs.getString("Query"));
                objExecutedQueryBean.setQueryDate(rs.getString("Query_Date"));

                alstAllExecutedQueries.add(objExecutedQueryBean);
            }

        } catch (Exception e) {
            System.out.println("getAllExecutedQueryListByUsername(int User_Id)  of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllExecutedQueryListByUsername(int User_Id)  of DBOperations: " + e);
            }
        }
        return alstAllExecutedQueries;
    }
    
    
    /**
     * This method shows the List of all the executed queries by date,it returns the reference of type ArrayList
     * @param date
     * @return 
     */
    public ArrayList getAllExecutedQueryListByDate(String date) {

        Connection conn = null;
        ArrayList alstAllExecutedQueries = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select Query_Activity_Id,uqa.User_Id ,Query,Query_Date from userqueryactivity uqa,usermaster um where um.User_Id=uqa.User_Id and uqa.Query_Date like ?");
            pstmt.setString(1, date+"%");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                UserQueryActivityBean objExecutedQueryBean = new UserQueryActivityBean();

                objExecutedQueryBean.setQueryId(rs.getString("Query_Activity_Id"));
                objExecutedQueryBean.setUserId(rs.getString("User_Id"));
                objExecutedQueryBean.setQuery(rs.getString("Query"));
                objExecutedQueryBean.setQueryDate(rs.getString("Query_Date"));

                alstAllExecutedQueries.add(objExecutedQueryBean);
            }

        } catch (Exception e) {
            System.out.println("getAllExecutedQueryListByDate(String date)  of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllExecutedQueryListByDate(String date)  of DBOperations: " + e);
            }
        }
        return alstAllExecutedQueries;
    }
    /**
     * This method shows the List of all the executed queries by USername and Date,it returns the reference of type ArrayList
     * @param username
     * @param date
     * @return 
     */
    public ArrayList getAllExecutedQueryListByUsernameDate(String username,String date) {

        Connection conn = null;
        ArrayList alstAllExecutedQueries = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select Query_Activity_Id,uqa.User_Id ,Query,Query_Date from userqueryactivity uqa,usermaster um where um.User_Id=uqa.User_Id and um.username=? and  uqa.Query_Date like ?");
            pstmt.setString(1, username);
            pstmt.setString(2, date+"%");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                UserQueryActivityBean objExecutedQueryBean = new UserQueryActivityBean();

                objExecutedQueryBean.setQueryId(rs.getString("Query_Activity_Id"));
                objExecutedQueryBean.setUserId(rs.getString("User_Id"));
                objExecutedQueryBean.setQuery(rs.getString("Query"));
                objExecutedQueryBean.setQueryDate(rs.getString("Query_Date"));

                alstAllExecutedQueries.add(objExecutedQueryBean);
            }

        } catch (Exception e) {
            System.out.println("getAllExecutedQueryListByUsernameDate(String username,String date)  of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
              System.out.println("getAllExecutedQueryListByUsernameDate(String username,String date)  of DBOperations: " + e);
            }
        }
        return alstAllExecutedQueries;
    }
    //---------------------------------------------------------------------------------------
    //          User Saved Query Related ExecutedQuery.java
    //-----------------------------------------------------------------------------------------------
/**
     * This method gives the list Of all the saved queries,it returns the reference of type ArrayList
     * @return 
     */
    public ArrayList getAllSavedQueryList() {

        Connection conn = null;
        ArrayList alstAllSavedQueries = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from querymaster");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                QueryMasterBean objBean = new QueryMasterBean();

                objBean.setQueryId(rs.getInt("Query_Id"));
                objBean.setUserId(rs.getInt("User_Id"));
                objBean.setQuery(rs.getString("Query"));
                objBean.setCreateDate(rs.getString("Create_Date"));

                alstAllSavedQueries.add(objBean);
            }

        } catch (Exception e) {
            System.out.println("getAllSavedQueryList() of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllSavedQueryList() of DBOperations: " + e);
            }
        }
        return alstAllSavedQueries;
    }
/**
     * This method gives the list of all saved queries by User_Id,it returns integer value
     * @param User_Id
     * @return integer
     */
    public ArrayList getAllSavedQueryListByUserId(int User_Id) {

        Connection conn = null;
        ArrayList alstAllSavedQueries = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select * from querymaster where User_Id=?");
            pstmt.setInt(1, User_Id);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                QueryMasterBean objBean = new QueryMasterBean();
                objBean.setQueryId(rs.getInt("Query_Id"));
                objBean.setUserId(rs.getInt("User_Id"));
                objBean.setQuery(rs.getString("Query"));
                objBean.setCreateDate(rs.getString("Create_Date"));

                alstAllSavedQueries.add(objBean);
            }

        } catch (Exception e) {
            System.out.println("getAllSavedQueryListByUserId() of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllSavedQueryListByUserId() of DBOperations: " + e);
            }
        }
        return alstAllSavedQueries;
    }
    
    /**
     * This method gives the list of all the saved queries by username,it returns the reference of type ArrayList
     * @param Username
     * @return ArrayList
     */
    public ArrayList getAllSavedQueryListByUserName(String Username) {

        Connection conn = null;
        ArrayList alstAllSavedQueries = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select Query_Id,qm.User_Id ,Query,Create_Date from querymaster qm,usermaster um where um.User_Id=qm.User_Id and um.username=?");
            pstmt.setString(1, Username);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                QueryMasterBean objBean = new QueryMasterBean();
                objBean.setQueryId(rs.getInt("Query_Id"));
                objBean.setUserId(rs.getInt("User_Id"));
                objBean.setQuery(rs.getString("Query"));
                objBean.setCreateDate(rs.getString("Create_Date"));

                alstAllSavedQueries.add(objBean);
            }

        } catch (Exception e) {
            System.out.println("getAllSavedQueryListByUserName() of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllSavedQueryListByUserName() of DBOperations: " + e);
            }
        }
        return alstAllSavedQueries;
    }
    /**
     * This method gives the list of all the saved queries by Date
     * @param date
     * @return ArrayList
     */
    
    
    public ArrayList getAllSavedQueryListByDate(String date) {

        Connection conn = null;
        ArrayList alstAllSavedQueries = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select Query_Id,qm.User_Id ,Query,Create_Date from querymaster qm,usermaster um where um.User_Id=qm.User_Id and qm.Create_Date like ?");
            pstmt.setString(1, date+"%");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                QueryMasterBean objBean = new QueryMasterBean();
                objBean.setQueryId(rs.getInt("Query_Id"));
                objBean.setUserId(rs.getInt("User_Id"));
                objBean.setQuery(rs.getString("Query"));
                objBean.setCreateDate(rs.getString("Create_Date"));

                alstAllSavedQueries.add(objBean);
            }

        } catch (Exception e) {
            System.out.println("getAllSavedQueryListByDate() of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllSavedQueryListByDate() of DBOperations: " + e);
            }
        }
        return alstAllSavedQueries;
    }
    
    /**
     * This method gives the list of all the saved queries by username and date
     * @param Username
     * @param date
     * @return ArrayList
     */
  public ArrayList getAllSavedQueryListByUserNameDate(String Username,String date) {

        Connection conn = null;
        ArrayList alstAllSavedQueries = new ArrayList();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("select Query_Id,qm.User_Id ,Query,Create_Date from querymaster qm,usermaster um where um.User_Id=qm.User_Id and um.username=? and qm.Create_date=?");
            pstmt.setString(1, Username);
            pstmt.setString(2, date+"%");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                QueryMasterBean objBean = new QueryMasterBean();
                objBean.setQueryId(rs.getInt("Query_Id"));
                objBean.setUserId(rs.getInt("User_Id"));
                objBean.setQuery(rs.getString("Query"));
                objBean.setCreateDate(rs.getString("Create_Date"));

                alstAllSavedQueries.add(objBean);
            }

        } catch (Exception e) {
            System.out.println("getAllSavedQueryListByUserNameDate() of DBOperations: " + e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("getAllSavedQueryListByUserNameDate() of DBOperations: " + e);
            }
        }
        return alstAllSavedQueries;
    }
  
  /**
   * This method updates the record of the saved queries,it returns String
   * @param objBean
   * @return String
   */
    public String updateSavedQueryRecord(QueryMasterBean objBean) {

        Connection conn = null;
        PreparedStatement pstmt = null;
        String result = "failed";
        try {
            conn = DBConnection.getConnection();
            pstmt = conn.prepareStatement("update querymaster set Query=?, Create_Date=? where query_id=?");
            pstmt.setString(1, objBean.getQuery());
            pstmt.setString(2, objBean.getCreateDate());
            pstmt.setInt(3, objBean.getUserId());
            int i = pstmt.executeUpdate();
            if (i > 0) {
                result = "updated";
            }

        } catch (Exception e) {
            System.out.println("updateSavedQueryRecord(QueryMasterBean objBean) of DBOperations: " + e);
        } finally {
            try {

                pstmt.close();
                conn.close();
            } catch (Exception e) {
                System.out.println("updateSavedQueryRecord(QueryMasterBean objBean) of DBOperations: " + e);
            }
        }
        return result;
    }
    
    
    
    
    //---------------------------------------------------------------------------------------
    //          Common Methods
    //-----------------------------------------------------------------------------------------------
   
      /**
     * This method gives the current date of the system and returns the reference of type String
     * @return String
     */
    public String getCurrentDateTime() {
        java.util.Date dd = new java.util.Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

        String strDate = sdf.format(dd);
        return strDate;
    }
}
