/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smartquery.alpha;

/**
 *
 * @author student
 */
public class ConnectToServerBean {
    
    private String hostServer, port, userName, password;

    public String getHostServer() {
        return hostServer;
    }

    public void setHostServer(String hostServer) {
        this.hostServer = hostServer;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    
}
