/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package smartquery.alpha;



/**
 *
 * @author Administrator
 */
public class UserQueryActivityBean{
    private String userId;
    private String queryId;
    private String queryDate;
    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQueryDate() {
        return queryDate;
    }

    public void setQueryDate(String queryDate) {
        this.queryDate = queryDate;
    }

    public String getQueryId() {
        return queryId;
    }

    public void setQueryId(String queryId) {
        this.queryId = queryId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }



}
