/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smartquery.alpha;

/**
 *
 * @author admin
 */
public class UserPriviledgeMasterBean {

    private int userId;
    private boolean canRetrieve;
    private boolean canInsert;
    private boolean canUpdate;
    private boolean canDelete;
    private boolean canDrop;
    private boolean canCreate;
    private boolean canAlter;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isCanAlter() {
        return canAlter;
    }

    public void setCanAlter(boolean canAlter) {
        this.canAlter = canAlter;
    }

    public boolean isCanCreate() {
        return canCreate;
    }

    public void setCanCreate(boolean canCreate) {
        this.canCreate = canCreate;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public boolean isCanDrop() {
        return canDrop;
    }

    public void setCanDrop(boolean canDrop) {
        this.canDrop = canDrop;
    }

    public boolean isCanInsert() {
        return canInsert;
    }

    public void setCanInsert(boolean canInsert) {
        this.canInsert = canInsert;
    }

    public boolean isCanRetrieve() {
        return canRetrieve;
    }

    public void setCanRetrieve(boolean canRetrieve) {
        this.canRetrieve = canRetrieve;
    }

    public boolean isCanUpdate() {
        return canUpdate;
    }

    public void setCanUpdate(boolean canUpdate) {
        this.canUpdate = canUpdate;
    }
}
