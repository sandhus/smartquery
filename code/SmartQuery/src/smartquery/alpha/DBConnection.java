/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smartquery.alpha;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *This class contains only one static method that creates the connection with database
 */
public class DBConnection {
/**
     * This method returns the object of type Connection
     * @return Connection
     */
    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql:///smartquery","root","");

        } catch (Exception e) {
            System.out.println("Exception in getConnection og DBConnection : "+e);
        }
        return con;
    }
}
