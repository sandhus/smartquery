/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 */
package smartquery;

import smartquery.alpha.DBOperations;
import smartquery.alpha.QueryMasterBean;
import smartquery.report.JasperReportGenerator;
import java.awt.Toolkit;
import java.io.InputStream;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author admin
 */
public class SavedQueryReport extends javax.swing.JFrame implements ListSelectionListener {

    DBOperations objDB;
    ArrayList alstSavedQuery;
    ArrayList alstUserName;
    ListSelectionModel objListSelectionModel;

    /** Creates new form UserAccountReport */
    public SavedQueryReport() {
        initComponents();
        setSize(Toolkit.getDefaultToolkit().getScreenSize());
        objDB = new DBOperations();
        alstSavedQuery = objDB.getAllSavedQueryList();
        generateTable();


        alstUserName = objDB.getAllUserNameList();
        for (int i = 0; i < alstUserName.size(); i++) {
            ddlUsername.addItem(alstUserName.get(i));
        }

        setTitle("User Saved Query Report");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    void generateTable() {


        Object data[][] = new Object[alstSavedQuery.size()][4];
        for (int i = 0; i < alstSavedQuery.size(); i++) {
            QueryMasterBean objBean = (QueryMasterBean) alstSavedQuery.get(i);
            data[i][0] = objBean.getQueryId();
            data[i][1] = objBean.getUserId();
            data[i][2] = objBean.getQuery();
            data[i][3] = objBean.getCreateDate();

        }
        String header[] = {"Query ID", "User ID", "Query", "Date"};

        tblSavedQuery = new JTable(data, header);
        objListSelectionModel = tblSavedQuery.getSelectionModel();
        objListSelectionModel.addListSelectionListener(this);

        jScrollPane1.setViewportView(tblSavedQuery);

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblSavedQuery = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        ddlUsername = new javax.swing.JComboBox();
        btnList = new javax.swing.JButton();
        btnListAll = new javax.swing.JButton();
        txtDate = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        taOutput = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miPrint = new javax.swing.JMenuItem();
        miExit = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tblSavedQuery.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "User ID", "Username", "Query", "Date"
            }
        ));
        jScrollPane1.setViewportView(tblSavedQuery);

        jLabel1.setFont(new java.awt.Font("Courier New", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("User Saved Query Report");

        jLabel2.setText("Username");

        ddlUsername.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select Username" }));
        ddlUsername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ddlUsernameActionPerformed(evt);
            }
        });

        btnList.setText("List");
        btnList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListActionPerformed(evt);
            }
        });

        btnListAll.setText("List All");
        btnListAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListAllActionPerformed(evt);
            }
        });

        jLabel3.setText("Date");

        taOutput.setColumns(20);
        taOutput.setRows(5);
        jScrollPane2.setViewportView(taOutput);

        jMenu1.setText("File");

        miPrint.setText("Print");
        miPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miPrintActionPerformed(evt);
            }
        });
        jMenu1.add(miPrint);

        miExit.setText("Exit");
        miExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExitActionPerformed(evt);
            }
        });
        jMenu1.add(miExit);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1031, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1021, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ddlUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnList, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnListAll, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(161, 161, 161))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1031, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnList)
                    .addComponent(btnListAll)
                    .addComponent(jLabel2)
                    .addComponent(ddlUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 402, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListActionPerformed
        // TODO add your handling code here:
        String username = ddlUsername.getSelectedItem().toString();
        String date = txtDate.getText().trim();

        if (username.equalsIgnoreCase("Select Username") && date.length() == 0) {
            alstSavedQuery = objDB.getAllSavedQueryList();
        } else if(username.equalsIgnoreCase("Select Username") && date.length() > 0){
            String str[] = date.split("-");
            if (str.length != 3) {
                JOptionPane.showMessageDialog(null, "Invalid date format, use yyyy-mm-dd");
                return;
            }
            alstSavedQuery = objDB.getAllSavedQueryListByDate(txtDate.getText());
        }else if(!username.equalsIgnoreCase("Select Username") && date.trim().length() == 0)
        {
            alstSavedQuery = objDB.getAllSavedQueryListByUserName(username);

        }
        else{
            alstSavedQuery = objDB.getAllSavedQueryListByUserNameDate(username,date);
        }
        generateTable();
    }//GEN-LAST:event_btnListActionPerformed

    private void btnListAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListAllActionPerformed
        // TODO add your handling code here:
        alstSavedQuery = objDB.getAllSavedQueryList();
        generateTable();

    }//GEN-LAST:event_btnListAllActionPerformed

    private void miPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miPrintActionPerformed
        // TODO add your handling code here:
        InputStream designFileStream = getClass().getResourceAsStream("report/SavedQueryReport.jrxml");
        JasperReportGenerator jrg = new JasperReportGenerator(designFileStream);
    }//GEN-LAST:event_miPrintActionPerformed

    private void miExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExitActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_miExitActionPerformed

private void ddlUsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ddlUsernameActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_ddlUsernameActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new SavedQueryReport().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnList;
    private javax.swing.JButton btnListAll;
    private javax.swing.JComboBox ddlUsername;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenuItem miPrint;
    private javax.swing.JTextArea taOutput;
    private javax.swing.JTable tblSavedQuery;
    private javax.swing.JTextField txtDate;
    // End of variables declaration//GEN-END:variables

    public void valueChanged(ListSelectionEvent e) {
        taOutput.setText(tblSavedQuery.getValueAt(tblSavedQuery.getSelectedRow(), 2).toString());
    }
}
