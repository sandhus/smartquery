/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * MainFrame.java
 *
  */
package smartquery;

import smartquery.alpha.UsermasterBean;
import java.awt.Container;
import java.awt.Toolkit;

/**
 *
 * @author vinay
 */
public class MainFrame extends javax.swing.JFrame {

    UsermasterBean objUsermasterBean;
    static Container container;
    static int userId;

    /** Creates new form MainFrame */
    public MainFrame() {
        //initComponents();
        //setVisible(true);
    }

    public MainFrame(UsermasterBean objUsermasterBean) {
        initComponents();
        this.objUsermasterBean = objUsermasterBean;
        userId = objUsermasterBean.getUserId();
        setSize(Toolkit.getDefaultToolkit().getScreenSize());
        setResizable(false);
        container = getContentPane();
        if (objUsermasterBean.getUserType().equalsIgnoreCase("employee")) {
            mbMainFrame.remove(mnuUserMaintenance);
            mbMainFrame.remove(mnuReports);
        }
        

        setVisible(true);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mbMainFrame = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miAnalyzer = new javax.swing.JMenuItem();
        miLogout = new javax.swing.JMenuItem();
        mnuProfile = new javax.swing.JMenu();
        miProfileDetail = new javax.swing.JMenuItem();
        mnuUserMaintenance = new javax.swing.JMenu();
        miUserDetail = new javax.swing.JMenuItem();
        mnuQueryAnalyzer = new javax.swing.JMenu();
        miSavedQuery = new javax.swing.JMenuItem();
        miExecutedQuery = new javax.swing.JMenuItem();
        mnuReports = new javax.swing.JMenu();
        miUserReport = new javax.swing.JMenuItem();
        miUserPrivilegeReport = new javax.swing.JMenuItem();
        miSavedQueryReport = new javax.swing.JMenuItem();
        miExecutedQueryReport = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SmartQuery");

        jMenu1.setText("File");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        miAnalyzer.setText("Browser");
        miAnalyzer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miAnalyzerActionPerformed(evt);
            }
        });
        jMenu1.add(miAnalyzer);

        miLogout.setText("Logout");
        miLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miLogoutActionPerformed(evt);
            }
        });
        jMenu1.add(miLogout);

        mbMainFrame.add(jMenu1);

        mnuProfile.setText("Profile");

        miProfileDetail.setText("Profile Detail");
        miProfileDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miProfileDetailActionPerformed(evt);
            }
        });
        mnuProfile.add(miProfileDetail);

        mbMainFrame.add(mnuProfile);

        mnuUserMaintenance.setText("User Maintenance");

        miUserDetail.setText("User Detail");
        miUserDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miUserDetailActionPerformed(evt);
            }
        });
        mnuUserMaintenance.add(miUserDetail);

        mbMainFrame.add(mnuUserMaintenance);

        mnuQueryAnalyzer.setText("Query Maintenance");

        miSavedQuery.setText("Saved Query");
        miSavedQuery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSavedQueryActionPerformed(evt);
            }
        });
        mnuQueryAnalyzer.add(miSavedQuery);

        miExecutedQuery.setText("Executed Query");
        miExecutedQuery.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExecutedQueryActionPerformed(evt);
            }
        });
        mnuQueryAnalyzer.add(miExecutedQuery);

        mbMainFrame.add(mnuQueryAnalyzer);

        mnuReports.setText("Reports");

        miUserReport.setText("User Detail Report");
        miUserReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miUserReportActionPerformed(evt);
            }
        });
        mnuReports.add(miUserReport);

        miUserPrivilegeReport.setText("User Privilege Report");
        miUserPrivilegeReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miUserPrivilegeReportActionPerformed(evt);
            }
        });
        mnuReports.add(miUserPrivilegeReport);

        miSavedQueryReport.setText("Saved Query Report");
        miSavedQueryReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSavedQueryReportActionPerformed(evt);
            }
        });
        mnuReports.add(miSavedQueryReport);

        miExecutedQueryReport.setText("Executed Query Report");
        miExecutedQueryReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExecutedQueryReportActionPerformed(evt);
            }
        });
        mnuReports.add(miExecutedQueryReport);

        mbMainFrame.add(mnuReports);

        setJMenuBar(mbMainFrame);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 277, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void miUserDetailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miUserDetailActionPerformed
        // TODO add your handling code here:
        container.removeAll();
        UserDetail objUserDetail = new UserDetail();
        objUserDetail.setBounds(250, 100, 460, 500);
        container.setVisible(false);
        container.add(objUserDetail);
        container.setVisible(true);
}//GEN-LAST:event_miUserDetailActionPerformed

    private void miLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miLogoutActionPerformed
        // TODO add your handling code here:
        System.exit(0);
}//GEN-LAST:event_miLogoutActionPerformed

    private void miProfileDetailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miProfileDetailActionPerformed
        // TODO add your handling code here:
        container.removeAll();
        ProfileDetail objUserDetail = new ProfileDetail();
        objUserDetail.setBounds(250, 100, 420, 370);
        container.setVisible(false);
        container.add(objUserDetail);
        container.setVisible(true);
    }//GEN-LAST:event_miProfileDetailActionPerformed

private void miUserReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miUserReportActionPerformed
        // TODO add your handling code here:
        new UserReport();
}//GEN-LAST:event_miUserReportActionPerformed

private void miSavedQueryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSavedQueryActionPerformed
        // TODO add your handling code here:
         container.removeAll();
        SavedQuery obj = new SavedQuery();
        obj.setBounds(270, 80, 465, 297);
        container.setVisible(false);
        container.add(obj);
        container.setVisible(true);
}//GEN-LAST:event_miSavedQueryActionPerformed

private void miExecutedQueryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExecutedQueryActionPerformed
        // TODO add your handling code here:
        container.removeAll();
        ExecutedQuery obj = new ExecutedQuery();
        obj.setBounds(270, 80, 460, 320);
        container.setVisible(false);
        container.add(obj);
        container.setVisible(true);
}//GEN-LAST:event_miExecutedQueryActionPerformed

private void miUserPrivilegeReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miUserPrivilegeReportActionPerformed
        // TODO add your handling code here:
        new UserPrivilgeReport();
}//GEN-LAST:event_miUserPrivilegeReportActionPerformed

private void miSavedQueryReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSavedQueryReportActionPerformed
        // TODO add your handling code here:
        new SavedQueryReport();
}//GEN-LAST:event_miSavedQueryReportActionPerformed

private void miExecutedQueryReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExecutedQueryReportActionPerformed
        // TODO add your handling code here:
        new ExecutedQueryReport();
}//GEN-LAST:event_miExecutedQueryReportActionPerformed

private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jMenu1ActionPerformed

private void miAnalyzerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miAnalyzerActionPerformed
        // TODO add your handling code here:
        new ConnectServer();
}//GEN-LAST:event_miAnalyzerActionPerformed
 
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar mbMainFrame;
    private javax.swing.JMenuItem miAnalyzer;
    private javax.swing.JMenuItem miExecutedQuery;
    private javax.swing.JMenuItem miExecutedQueryReport;
    private javax.swing.JMenuItem miLogout;
    private javax.swing.JMenuItem miProfileDetail;
    private javax.swing.JMenuItem miSavedQuery;
    private javax.swing.JMenuItem miSavedQueryReport;
    private javax.swing.JMenuItem miUserDetail;
    private javax.swing.JMenuItem miUserPrivilegeReport;
    private javax.swing.JMenuItem miUserReport;
    private javax.swing.JMenu mnuProfile;
    private javax.swing.JMenu mnuQueryAnalyzer;
    private javax.swing.JMenu mnuReports;
    private javax.swing.JMenu mnuUserMaintenance;
    // End of variables declaration//GEN-END:variables
}
